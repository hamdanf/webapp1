﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class User
    {
        public string Name { get; set; }
        public string Age { get; set; }
        public string Address { get; set; }
    }
}
